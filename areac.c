#include<stdio.h>
#include<math.h>
//Area of triangle
//ask the users to enter the diamentions of the triangle
//ask the computer to store the values 
//ask the computer to calculate the semiperimeter
// ask the computer to calculate the area
//ask the computer to print the area of a triangle
int getA()
{
	int A;
	printf("enter the value of A\n");
	scanf("%d", &A);
	return A;
}

int getB()
{
	int B;
	printf("enter the value of B\n");
	scanf("%d", &B);
	return B;
}

int getC()
{
	int C;
	printf("enter the value of C\n");
	scanf("%d", &C);
	return C;
}

int calculateSemiPerimeter(int A, int B, int C)
{
	int S;
	S = (A + B + C) / 2;
	return S;
}

int calculateArea(int S, int A, int B, int C)
{
	int Area;
	Area = sqrt(S * (S - A) * (S - B) * (S - C));
	return Area;

}

void main(void)
{
	int A, B, C, S, Area;
	A = getA();
	B = getB();
	C = getC();
	S = calculateSemiPerimeter(A, B, C);
	Area = calculateArea(S, A, B, C);
	printf("%d", Area);
}